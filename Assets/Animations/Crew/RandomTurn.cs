﻿using UnityEngine;
using System.Collections;

public class RandomTurn : MonoBehaviour
{

		public float timeWait = 0;
		public bool isTurned = false;
		private Animator animator;
		// Use this for initialization
		void Start ()
		{
				timeWait = Random.Range (20, 100);
				animator = GetComponent<Animator> ();
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (timeWait <= 0) {
						isTurned = !isTurned;
						animator.SetBool ("Turn", isTurned);
						timeWait = Random.Range (5, 15);
				} else {
						timeWait -= Time.deltaTime;
				}	
		}
}
