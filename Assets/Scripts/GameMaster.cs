﻿using UnityEngine;
using System.Collections;

public class GameMaster : MonoBehaviour {

	public static GameMaster gm;

	void Start(){
		if(gm == null){
			gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameMaster>();
		}
	}


	public Transform playerPrefab;
	public Transform spawnPoint;
	public float spawnDelay = 2f;
	public Transform spawnParticlesPrefab;

	public IEnumerator RespawnPlayer(){
		// spawn audio
		audio.Play ();

		// spawn particles
		GameObject clone = Instantiate (spawnParticlesPrefab, spawnPoint.position, spawnPoint.rotation) as GameObject;

		// destroy spawn particles 
		Destroy (clone, 2f);

		yield return new WaitForSeconds (spawnDelay);

		// Spawns the player
		Instantiate (playerPrefab, spawnPoint.position, spawnPoint.rotation);
	}



	public static void KillPlayer(Player player){
		Destroy (player.gameObject); 
		gm.StartCoroutine(gm.RespawnPlayer());
	}
}
