﻿using UnityEngine;
using System.Collections;

public class Rotation : MonoBehaviour {

	public int rotationOffset = 180;
	public Transform target;
	public float updateRate = 4;


	void Start() {
		StartCoroutine (UpdateTarget());
	}


	// Update is called once per frame
	IEnumerator UpdateTarget () {
		yield return new WaitForSeconds (1f/updateRate);

		//subtracting the position of the player from the mouse position
		Vector3 difference = (target.position - transform.position);
		//Vector3 difference = (transform.position - target.position);
		difference.Normalize ();		// normalizing the vector, the sum of the vector will be equal to 1
		Debug.Log ("Diff x: " + difference.x + "   Diff y: " + difference.y);

		// Finding the angle and converting it to degrees
		//float rotZ = Mathf.Atan2 (difference.y, difference.x) * Mathf.Rad2Deg;
		float rotZ = Mathf.Atan2 (difference.x, difference.y) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.Euler (0f, 0f, rotZ + rotationOffset);
		float temp = rotZ + rotationOffset;
		Debug.Log ("rotZ: " + rotZ + "\n rotZ: " + temp);

		StartCoroutine (UpdateTarget());
	}
}
