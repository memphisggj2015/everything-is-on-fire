﻿using UnityEngine;
using System.Collections;

public class DamagedShip : MonoBehaviour
{


		private float angleHigh;
		private float angleLow;
		public float delta = .1f;
		private float currentAngle = 0f;
		private bool wobbleUp = true;

		private Vector3 shipOriginalPosition;

		// Use this for initialization
		void Start ()
		{
				angleHigh = Random.Range (0, 10);
				angleLow = Random.Range (-10, 0);
				shipOriginalPosition = transform.localPosition;

				tweenAgain ();
		}
	
		// Update is called once per frame
		void Update ()
		{

				if (wobbleUp) {
						if (currentAngle < angleHigh) {
								currentAngle += delta;
						} else {
								wobbleUp = false;
								angleHigh = Random.Range (0, 15);
						}
				} else {
						if (currentAngle > angleLow) {
								currentAngle -= delta;
						} else {
								wobbleUp = true;
								angleLow = Random.Range (-15, 0);
						}
				}
				Vector3 currentEulers = transform.rotation.eulerAngles;
				currentEulers.z = currentAngle;
				transform.rotation = Quaternion.Euler (currentEulers);
		}

		Vector3 GetRandomPosition ()
		{
				float x = Random.Range (shipOriginalPosition.x - 1, shipOriginalPosition.x + 1);
				float y = Random.Range (shipOriginalPosition.y - 1, shipOriginalPosition.y + 1);
				return new Vector3 (x, y, shipOriginalPosition.z);

		}

		private void tweenAgain ()
		{
				Vector3 nextPos = GetRandomPosition ();
				iTween.MoveTo (gameObject, iTween.Hash ("position", nextPos, "time", 5, "oncomplete", "tweenAgain", "islocal", true, "easetype", iTween.EaseType.easeInOutSine));

		}
	
}
